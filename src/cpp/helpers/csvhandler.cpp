//
// Created by hanshardmeier on 20.06.17.
//

#include "csvhandler.h"

namespace fs = boost::filesystem;
/**
 * Opens CSV to write. Deletes previous content if exists.
 * If the path doesnt exists, it is created.
 * @param csv_output_path Path of file
 * @param csv_output Pointer to fstream object
 */
void csv::open_or_create_CSVFile_to_write(
    boost::filesystem::path csv_output_path,
    std::fstream *csv_output) {
  pqry::Logging::say(__FILE__, __LINE__, "Opening CSV Stream");
  csv_output->open(csv_output_path.string(),
                  std::fstream::out | std::fstream::trunc);

  if (!csv_output->is_open()) {
    pqry::Logging::say(__FILE__, __LINE__, "CSV File cannot be opened. Creating parent folder");
    fs::path parent = csv_output_path.parent_path();
    if (!fs::create_directories(parent.string()))
      pqry::Logging::panic(__FILE__, __LINE__,
                           "Can't create parent folder %s.",parent.string());
    csv_output->open(csv_output_path.string(), std::fstream::out);
    if (!csv_output->is_open())
      pqry::Logging::panic(__FILE__, __LINE__,
                           "Can't open file or Create %s.",
                           csv_output_path.string());
  }
}



/**
 * Writes the section of detections for the given image
 * @param file Output stream
 * @param img_path Path of image
 * @param detections Vector of detections
 */
void csv::write_detections(std::fstream* file, boost::filesystem::path img_path,
                      std::vector<Detection> detections) {
  pqry::Logging::say(__FILE__, __LINE__,"Writting detections");
  (*file) << img_path.string() << std::endl;

  for (std::vector<Detection>::iterator det_it = detections.begin();
       det_it != detections.end(); ++det_it) {
    Detection det = *det_it;

    (*file) << det.class_id << "," << det.bbox.x << ","
         << det.bbox.y
         << "," << det.bbox.width << "," << det.bbox.height
         << std::endl;
  }
}

/**
 * Opens the CSV file to read the input and initiliazes the input stream.
 *
 * @param csv_output_path Path to CSV File
 * @param csv_input Input stream
 */
void csv::open_CSVFile_to_read(boost::filesystem::path csv_output_path,
                               std::fstream *csv_input){
  csv_input->open(csv_output_path.string(), std::fstream::in);
  if (!csv_input->is_open())
    pqry::Logging::panic(__FILE__, __LINE__, "CSV File %s cannot be opened.",csv_output_path.string());
}

/**
 * Parses the content of the CSV Input file into a vector of pairs with the img path and the vector of detections.
 *
 * @param csv_input Input stream
 * @return Parsed Data
 */
std::vector<std::pair<fs::path,std::vector<Detection>>> csv::parseCSVinput(std::fstream  *csv_input){
  std::vector<std::pair<fs::path,std::vector<Detection>>> data;
  std::string line;
  int idx=0;

  fs::path img_path;
  std::streampos oldpos;

  while(std::getline(*csv_input,line)){
    std::vector<Detection> detections;

    if(std::isdigit(line[0]))
      pqry::Logging::panic(__FILE__, __LINE__, "Image path expected");

    img_path = fs::path(line);

    while(std::getline(*csv_input,line)){
      if(std::isdigit(line[0])){
        std::istringstream ss(line);
        Detection det;

        ss>> det.class_id;
        ss.ignore();
        ss>>det.bbox.x;
        ss.ignore();
        ss>>det.bbox.y;
        ss.ignore();
        ss>>det.bbox.width;
        ss.ignore();
        ss>>det.bbox.height;

        detections.push_back(det);
        oldpos = (*csv_input).tellg();
      }else{
        (*csv_input).seekg(oldpos);
        data.push_back(std::pair<fs::path,std::vector<Detection>>(img_path,detections));
        break;
      }
    }
  }
  return data;
}