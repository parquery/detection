//
// Created by hanshardmeier on 20.06.17.
//

#ifndef DETECTION_CSVHANDLER_H
#define DETECTION_CSVHANDLER_H


#include "logging/logging.h"
#include "detection/detector.h"
#include <fstream>
#include <iostream>
#include <string>

namespace csv{
  void open_or_create_CSVFile_to_write(boost::filesystem::path csv_output_path,
                                       std::fstream *csv_output);
  void write_detections(std::fstream* file, boost::filesystem::path img_path,std::vector<Detection> detections);
  void open_CSVFile_to_read(boost::filesystem::path csv_output_path,
                            std::fstream *csv_input);

  std::vector<std::pair<boost::filesystem::path,std::vector<Detection>>> parseCSVinput(std::fstream *csv_input);
}
#endif //DETECTION_CSVHANDLER_H
