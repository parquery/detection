/*
* Copyright 2017 Parquery AG <Hans Hardmeier>
*/

#ifndef SRC_CPP_DETECTION_HELPER_UTILS_H_
#define SRC_CPP_DETECTION_HELPER_UTILS_H_

#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

namespace Utils {
    bool file_exists(const std::string &name);

    bool isDirectory(const char *path);

    bool
    in_array(const std::string &value, const std::vector<std::string> &array);
    void free_all_ptrs(void **ptrs, int n);
    int max_index_in_array(float *a, int n);
}  // namespace Utils

#endif  // SRC_CPP_DETECTION_HELPER_UTILS_H_
