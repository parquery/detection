/*
* Copyright 2017 Parquery AG <Hans Hardmeier>
 *
 * This class is a CPP Wrapper and Parser for the Darknet-YOLO:
 * Real-Time Object Detection (https://pjreddie.com/darknet/yolo/).
 *
 * This interfaces limits and simplifies the interface to train, validate and
 * use the Darknet-Network.
 *
*/

#include "darknetWrapper.h"

extern "C" {
#include "third_party/darknet/src/utils.h"

void set_global_paths(char *data_file);

//void full_detector(char *datacfg, char *cfgfile,
//                   char *weightfile, char **filenames, int numfilenames,
//                   float thresh, float hier_thresh,
//                   char *outfile);
//void full_display(char *datacfg, char *detection_file, char *output_folder);
void run_detector(int argc, char **argv,char** filenames, int nfilenames);
}

DarknetWrapper::DarknetWrapper(char *datafile) {
  if (!Utils::file_exists(datafile)) {
    std::fprintf(stderr, ".data File was not found \n");
    exit(1);
  }

  std::fprintf(stderr, "Loading .data File from: %s \n", datafile);
  set_global_paths(datafile);
}

/**
 * Usage ./detectory Data-File Cfg-File Weights-File Img"
 *
 * Flags:
 *
 * -prefix (unused)
 *
 * -thresh 0.4
 *      Defines threshold of network to filter detections
 *
 * -hier (unclear)
 *      Defines threshold for tree in BBox predictions
 *
 * -outTxt path/output.txt
 *      Defines the path and name of the Txt-Output-File. Else the output
 *      is written in predOutput.txt in the current Folder.
 *
 * -outImgDir path_dir/
 *      If variable defined, images with the BBoxes will be created
 *      in the defined folder.
 *
 * -fullscreen 1/0
 *      If OpenCV is used, FULLSCREEN Mode is used for the images. REQUIRES
 *      -outImgDir to be set.
 *
 * @param argc >= 5
 * @param argv See Above
 */
void DarknetWrapper::detect(int argc, char **argv) {

  if(argc<5){
    std::fprintf(stderr, "Wrong number of arguments. "
            "At least %d args needed. \n",argc);
    return;
  }


  char **filenames = reinterpret_cast<char **>(calloc(1, sizeof(char *)));
  filenames[0] = argv[4];

  int n_argc = argc - 1 + 3;
  char **n_argv = reinterpret_cast<char **>(calloc(n_argc, sizeof(char *)));
  n_argv[0] = const_cast<char *>("./darknet");
  n_argv[1] = const_cast<char *>("detector");
  n_argv[2] = const_cast<char *>("full");
  for (int i = 1; i < argc; ++i) {
    n_argv[2 + i] = i==4?0:argv[i]; //Skipping ImgFile
  }

  run_detector(n_argc, n_argv,filenames,1);
}

/**
 * Detection-TxtFile Input -> OutImage with BoundaryBoxes of predictions
 *
 * Flags:
 *
 * -prefix (unused)
 *
 * -thresh 0.4
 *      Defines threshold of network to filter detections
 *
 * -hier (unclear)
 *      Defines threshold for tree in BBox predictions
 *
 * -out path/output.txt
 *      Defines the path and name of the Txt-Output-File. Else the output
 *      is written in predOutput.txt in the current Folder.
 *
 * -outImgDir path_dir/
 *      If variable defined, images with the BBoxes will be created
 *      in the defined folder.
 *
 * -fullscreen 1/0
 *      If OpenCV is used, FULLSCREEN Mode is used for the images. REQUIRES
 *      -outImgDir to be set.
 *
 * @param argc >= 5
 * @param argv See Above
 */
void DarknetWrapper::display(int argc, char **argv) {
  char **filenames =reinterpret_cast<char **>(calloc(1, sizeof(char *)));
  filenames[0] = argv[2];

  int n_argc = argc - 1 + 3;
  char **n_argv = reinterpret_cast<char **>(calloc(n_argc, sizeof(char *)));
  n_argv[0] = const_cast<char *>("./darknet");
  n_argv[1] = const_cast<char *>("detector");
  n_argv[2] = const_cast<char *>("display");
  for (int i = 1; i < argc; ++i) {
    n_argv[2 + i] = i==2?(char*)" ":argv[i]; //Skipping TxtFile
    printf("%s \n",argv[i]);
  }

  run_detector(n_argc, n_argv,filenames,1);
}

void DarknetWrapper::detectFolder(int argc, char **argv) {
  std::string dirPath = argv[4];

  // Iterate of dir to get all images
  std::vector<char *> cfilenames;
  std::vector<std::string> formats{"jpg", "png", "jpeg"};
  DIR *dir;
  struct dirent *dp;
  char *name;
  dir = opendir(dirPath.c_str());
  while ((dp = readdir(dir)) != NULL) {
    if (!strcmp(dp->d_name, ".") || !strcmp(dp->d_name, "..")) {
      // do nothing (straight logic)
    } else {
      name = dp->d_name;  // use it
      std::string nameS(name);
      std::string extension = nameS.substr(nameS.find_last_of(".") + 1);
      if (Utils::in_array(extension, formats)) {
        std::fprintf(stderr, "Image found: %s", name);
        cfilenames.push_back(strdup((dirPath + name).c_str()));
      }
    }
  }
  closedir(dir);

  //full_detector(argv[1], argv[2], argv[3], &cfilenames[0],
  //              cfilenames.size(), thresh, .5, outFile);


  int n_argc = argc - 1 + 3;
  char **n_argv = reinterpret_cast<char **>(calloc(n_argc, sizeof(char *)));
  n_argv[0] = const_cast<char *>("./darknet");
  n_argv[1] = const_cast<char *>("detector");
  n_argv[2] = const_cast<char *>("full");
  for (int i = 1; i < argc; ++i) {
    n_argv[2 + i] = i==4?0:argv[i]; //Skipping InputDir
  }


  run_detector(n_argc, n_argv,&cfilenames[0],cfilenames.size());
}


void DarknetWrapper::train(int argc, char **argv) {
  // parse args into darknet arguments
  // ./traintory data cfg weights(optional) -gpus 0,1,2,3

  int n_argc = argc - 1 + 3;
  char **n_argv = reinterpret_cast<char **>(calloc(n_argc, sizeof(char *)));
  n_argv[0] = const_cast<char *>("./darknet");
  n_argv[1] = const_cast<char *>("detector");
  n_argv[2] = const_cast<char *>("train");
  for (int i = 1; i < argc; ++i) {
    n_argv[2 + i] = argv[i];
  }
  run_detector(n_argc, n_argv,0,0);
}

/**
 * Calculates several validation benchmarks for the calculated weights using
 * the data in DATA-File and Cfg-File.
 *
 * Usage ./validatory Data-File Cfg-File Weights
 *
 * @param argc
 * @param argv
 * @return void
 */
void DarknetWrapper::validation(int argc, char **argv) {
  int n_argc = argc - 1 + 3;
  char **n_argv = reinterpret_cast<char **>(calloc(n_argc, sizeof(char *)));
  n_argv[0] = const_cast<char *>("./darknet");
  n_argv[1] = const_cast<char *>("detector");
  n_argv[2] = const_cast<char *>("valid");
  for (int i = 1; i < argc; ++i) {
    n_argv[2 + i] = argv[i];
  }
  //run_detector(n_argc, n_argv,0,0);
  // n_argv[2]="valid2";
  // run_detector(n_argc, n_argv); //- SegFault in Network
  n_argv[2] = const_cast<char *>("recall");
  run_detector(n_argc, n_argv,0,0);
}

DarknetWrapper::~DarknetWrapper() {
  std::fprintf(stderr, "~DarknetWrapper() \n");
}
