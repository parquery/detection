// Copyright (c) 2016 Parquery AG. All rights reserved.
// Created by mristin on 10/31/16.

#pragma once

#include <ctime>
#include <string>

namespace pqry {

std::string strftime(const std::string& format, const struct tm& tm);

/**
 * @param format of the date/time
 * @param t seconds since epoch in UTC
 * @return t formatted according to `format`
 */
std::string strftime(const std::string& format, time_t t);

/**
 * @param t seconds since epoch in UTC
 * @return t formatted according to %Y-%m-%d %H:%M:%SZ
 */
std::string strftime(time_t t);

/**
 * @param t seconds since epoch in UTC
 * @return t formatted according to %Y%m%dT%H%M%SZ s.t. you can insert it into file paths
 */
std::string strftime_for_path(time_t t);

/**
 * Wrapper around gmtime
 * @param t seconds since epoch in UTC
 * @return tm in UTC
 */
struct tm gmtime(time_t t);

/**
 * Converts a UTC datetime to a timestamp.
 *
 * @param year
 * @param month
 * @param day
 * @param hour
 * @param minute
 * @param second
 * @return timestamp as seconds since epoch in UTC
 */
time_t datetime(int year, int month, int day, int hour = 0, int minute = 0, int second = 0);

}  // namespace pqry
