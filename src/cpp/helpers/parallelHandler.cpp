//
// Created by hanshardmeier on 22.06.17.
//

#include "parallelHandler.h"

/**
 * Initializes the Threadpool with nthreads num of threads
 * @param nthreads
 */
void ParallelHandler::init(int nthreads) {

  mWork = boost::in_place(boost::ref(ioService));

  for (int i = nthreads; i > 0; --i) {
    threadpool.create_thread(boost::bind(&boost::asio::io_service::run, &ioService));
  }
}

/**
 * Gets ioService to queue Jobs into the threadpool.
 *
 * (Ideally you would pass the function as an argument instead of giving the whole service back.)
 * @return
 */
boost::asio::io_service* ParallelHandler::getLoopService(){
  return &ioService;
}

/**
 * Executes all the jobs parallel and blocks until done.
 */
void ParallelHandler::executeAll(){
  mWork = boost::none;
  //ioService.run();
  threadpool.join_all();
}
