//
// Created by hanshardmeier on 22.06.17.
//

#ifndef DETECTION_PARALLELHANDLER_H
#define DETECTION_PARALLELHANDLER_H

#include <boost/asio/io_service.hpp>
#include <boost/utility/in_place_factory.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>
#include <boost/optional/optional.hpp>

class ParallelHandler{

private:
    boost::asio::io_service ioService;
    boost::thread_group threadpool;
    boost::optional<boost::asio::io_service::work> mWork;
public:
    void init(int nthreads);

    boost::asio::io_service*  getLoopService();
    void executeAll();

};


#endif //DETECTION_PARALLELHANDLER_H
