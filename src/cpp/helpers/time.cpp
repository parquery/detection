// Copyright (c) 2016 Parquery AG. All rights reserved.
// Created by mristin on 10/31/16.

#include "helpers/time.h"

#include <third_party/date/date.h>
#include <third_party/date/tz.h>

#include <ctime>
#include <sstream>

std::string pqry::strftime(const std::string& format, const struct tm& tm) {
  char buf[256];
  std::strftime(buf, 256, format.c_str(), &tm);
  return std::string(buf);
}

std::string pqry::strftime(const std::string& format, time_t t) { return pqry::strftime(format, pqry::gmtime(t)); }

std::string pqry::strftime(time_t t) { return pqry::strftime("%Y-%m-%d %H:%M:%SZ", t); }

std::string pqry::strftime_for_path(time_t t) { return pqry::strftime("%Y%m%dT%H%M%SZ", t); }

struct tm pqry::gmtime(time_t t) {
  struct tm tm = {};
  auto* ret = gmtime_r(&t, &tm);
  if (ret == nullptr) {
    // we may not use pqry::Logging::panic since there would be a circular
    // dependency.
    std::stringstream ss;
    ss << __FILE__ << ": " << __LINE__ << ": gmtime_r failed";
    throw std::runtime_error(ss.str());
  }

  return tm;
}

time_t pqry::datetime(int year, int month, int day, int hour, int minute, int second) {
  const auto midnight = date::floor<std::chrono::seconds>(date::sys_days(date::year(year) / month / day));

  return midnight.time_since_epoch().count() + hour * 3600 + minute * 60 + second;
}
