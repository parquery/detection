/*
* Copyright 2017 Parquery AG <Hans Hardmeier>
*/

#ifndef SRC_CPP_DETECTION_HELPER_DARKNETWRAPPER_H_
#define SRC_CPP_DETECTION_HELPER_DARKNETWRAPPER_H_

#pragma once

#include <limits.h>
#include <string.h>

#include <dirent.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "utils.h"

class DarknetWrapper {
public:
    explicit DarknetWrapper(char *datafile);

    ~DarknetWrapper();

    void detect(int argc, char **argv);

    void detectFolder(int argc, char **argv);

    void train(int argc, char **argv);

    void validation(int argc, char **argv);

    void display(int argc, char **argv);
};

#endif  // SRC_CPP_DETECTION_HELPER_DARKNETWRAPPER_H_
