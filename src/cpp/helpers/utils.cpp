/*
*  Copyright 2017 Parquery AG <Hans Hardmeier>
*/

#include "utils.h"

namespace Utils {
bool file_exists(const std::string &name) {
  struct stat buffer;
  return (stat(name.c_str(), &buffer) == 0);
}

bool isDirectory(const char *path) {
  struct stat statbuf;
  if (stat(path, &statbuf) != 0)
    return false;
  return S_ISDIR(statbuf.st_mode) != 0;
}

bool in_array(const std::string &value, const std::vector<std::string> &array) {
  return std::find(array.begin(), array.end(), value) != array.end();
}

  void free_all_ptrs(void **ptrs, int n)
  {
    int i;
    for(i = 0; i < n; ++i) free(ptrs[i]);
    free(ptrs);
  }


    int max_index_in_array(float *a, int n)
    {
      if(n <= 0) return -1;
      int i, max_i = 0;
      float max = a[0];
      for(i = 1; i < n; ++i){
        if(a[i] > max){
          max = a[i];
          max_i = i;
        }
      }
      return max_i;
    }

} // namespace Utils
