// Copyright 2016 Parquery AG

#include "logging/logging.h"

#include <boost/filesystem/path.hpp>

namespace pqry {
namespace logging {

std::string short_path(const std::string &filepath) {
  std::string pth = filepath;

  // try to shorten the path
  size_t marker = pth.find("production/src/cpp/");
  if (marker != pth.npos) {
    pth = pth.substr(marker + strlen("production/src/cpp/"));
  }

  marker = pth.find("/pqry/src/cpp/");
  if (marker != pth.npos) {
    pth = pth.substr(marker + strlen("/pqry/src/cpp/"));
  }

  return pth;
}

namespace impl {
const char *const kConsoleDefault = "\e[0m";
const char *const kForeBlack = "\e[30m";
const char *const kForeBlue = "\e[34m";
const char *const kForeRed = "\e[31m";
const char *const kForeMagenta = "\e[35m";
const char *const kForeGreen = "\e[32m";
const char *const kForeCyan = "\e[36m";
const char *const kForeYellow = "\e[33m";
const char *const kForeWhite = "\e[37m";
const char *const kForeConsole = "\e[39m";

const char *const kForelightBlack = "\e[90m";
const char *const kForelightBlue = "\e[94m";
const char *const kForelightRed = "\e[91m";
const char *const kForelightMagenta = "\e[95m";
const char *const kForelightGreen = "\e[92m";
const char *const kForelightCyan = "\e[96m";
const char *const kForelightYellow = "\e[93m";
const char *const kForelightWhite = "\e[97m";

const char *const kBackBlack = "\e[40m";
const char *const kBackBlue = "\e[44m";
const char *const kBackRed = "\e[41m";
const char *const kBackMagenta = "\e[45m";
const char *const kBackGreen = "\e[42m";
const char *const kBackCyan = "\e[46m";
const char *const kBackYellow = "\e[43m";
const char *const kBackWhite = "\e[47m";
const char *const kBackConsole = "\e[49m";

const char *const kBacklightBlack = "\e[100m";
const char *const kBacklightBlue = "\e[104m";
const char *const kBacklightRed = "\e[101m";
const char *const kBacklightMagenta = "\e[105m";
const char *const kBacklightGreen = "\e[102m";
const char *const kBacklightCyan = "\e[106m";
const char *const kBacklightYellow = "\e[103m";
const char *const kBacklightWhite = "\e[107m";

void prefix(const char *filepath, int line, std::ostream *out, bool isError) {
  const time_t t = ::time(nullptr);

  *out << (isError ? impl::kForeRed : impl::kForeBlue) << std::left
       << std::setw(26) << (pqry::logging::short_path(filepath) + ": ")
       << std::right << std::setw(5) << line << ": "
       << pqry::strftime("%Y-%m-%d %H:%M:%SZ", t) << ": "
       << impl::kConsoleDefault;
}
} // namespace impl
} // namespace logging
} // namespace pqry

std::ostream *pqry::Logging::out_ = &std::cout; // NOLINT

std::mutex pqry::Logging::out_mu_;

void pqry::Logging::set_out(std::ostream *out) {
  std::lock_guard<std::mutex> guard(pqry::Logging::out_mu_);
  pqry::Logging::out_ = out;
}

void pqry::Logging::say(const char *filepath, int line,
                        const std::string &message) {
  std::lock_guard<std::mutex> guard(pqry::Logging::out_mu_);
  pqry::logging::impl::prefix(filepath, line, out_, false);
  *out_ << message << std::endl;
}

void pqry::Logging::panic(const char *filepath, int line,
                          const std::string &message) {
  std::stringstream ss;
  pqry::logging::impl::prefix(filepath, line, &ss, true);
  ss << message;
  throw std::runtime_error(ss.str());
}
