// Copyright 2016 Parquery AG

#pragma once

#include "helpers/time.h"

#include <boost/filesystem/path.hpp>
#include <boost/format.hpp>

#include <cstring>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <mutex>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>

namespace pqry {
namespace logging {

/**
 * Shortens the path based on pre-defined markers (such as "production/src/cpp").
 * @param filepath to the source code
 * @return shortened path (or the original path if the markers could not be found)
 */
std::string short_path(const std::string& filepath);

namespace impl {
void prefix(const char* filepath, int line, std::ostream* out,bool isError);
}  // namespace impl
}  // namespace logging

class Logging {
 public:
  static void set_out(std::ostream* out);

  static void say(const char* filepath, int line, const std::string& message);

  template <typename T0>
  static void say(const char* filepath, int line, const std::string& format, const T0& a0) {
    std::lock_guard<std::mutex> guard(out_mu_);
    pqry::logging::impl::prefix(filepath, line, out_,false);
    *out_ << boost::format(format) % a0;
    *out_ << std::endl;
  }

  template <typename T0, typename T1>
  static void say(const char* filepath, int line, const std::string& format, const T0& a0, const T1& a1) {
    std::lock_guard<std::mutex> guard(out_mu_);
    pqry::logging::impl::prefix(filepath, line, out_,false);
    *out_ << boost::format(format) % a0 % a1;
    *out_ << std::endl;
  }

  template <typename T0, typename T1, typename T2>
  static void say(const char* filepath, int line, const std::string& format, const T0& a0, const T1& a1, const T2& a2) {
    std::lock_guard<std::mutex> guard(out_mu_);
    pqry::logging::impl::prefix(filepath, line, out_,false);
    *out_ << boost::format(format) % a0 % a1 % a2;
    *out_ << std::endl;
  }

  template <typename T0, typename T1, typename T2, typename T3>
  static void say(
      const char* filepath, int line, const std::string& format,
      const T0& a0, const T1& a1, const T2& a2, const T3& a3) {
    std::lock_guard<std::mutex> guard(out_mu_);
    pqry::logging::impl::prefix(filepath, line, out_,false);
    *out_ << boost::format(format) % a0 % a1 % a2 % a3;
    *out_ << std::endl;
  }

  static void panic(const char* filepath, int line, const std::string& message);

  template <typename T0>
  static void panic(const char* filepath, int line, const std::string& format, const T0& a0) {
    std::stringstream ss;
    pqry::logging::impl::prefix(filepath, line, &ss, true);
    ss << boost::format(format) % a0;
    throw std::runtime_error(ss.str());
  }

  template <typename T0, typename T1>
  static void panic(const char* filepath, int line, const std::string& format, const T0& a0, const T1& a1) {
    std::stringstream ss;
    pqry::logging::impl::prefix(filepath, line, &ss, true);
    ss << boost::format(format) % a0 % a1;
    throw std::runtime_error(ss.str());
  }

  template <typename T0, typename T1, typename T2>
  static void panic(const char* filepath, int line, const std::string& format, const T0& a0, const T1& a1,
                    const T2& a2) {
    std::stringstream ss;
    pqry::logging::impl::prefix(filepath, line, &ss, true);
    ss << boost::format(format) % a0 % a1 % a2;
    throw std::runtime_error(ss.str());
  }

  template <typename T0, typename T1, typename T2, typename T3>
  static void panic(const char* filepath, int line, const std::string& format, const T0& a0, const T1& a1,
                    const T2& a2, const T3& a3) {
    std::stringstream ss;
    pqry::logging::impl::prefix(filepath, line, &ss, true);
    ss << boost::format(format) % a0 % a1 % a2 % a3;
    throw std::runtime_error(ss.str());
  }

  template <typename T0, typename T1, typename T2, typename T3, typename T4>
  static void panic(const char* filepath, int line, const std::string& format, const T0& a0, const T1& a1,
                    const T2& a2, const T3& a3, const T4& a4) {
    std::stringstream ss;
    pqry::logging::impl::prefix(filepath, line, &ss, true);
    ss << boost::format(format) % a0 % a1 % a2 % a3 % a4;
    throw std::runtime_error(ss.str());
  }

 private:
  static std::ostream* out_;
  static std::mutex out_mu_;

  Logging() = default;
};
}  // namespace pqry
