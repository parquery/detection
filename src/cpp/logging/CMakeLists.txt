add_library(pqry_logging
        SHARED
        logging.h
        logging.cpp)
target_link_libraries(pqry_logging pqry_time boost_system)
install(TARGETS pqry_logging DESTINATION pqry_lib)
