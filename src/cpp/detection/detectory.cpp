/*
* Copyright 2017 Parquery AG <Hans Hardmeier>
*/

#include <unistd.h>

#include <iostream>
#include <string>

#include "helpers/darknetWrapper.h"
#include "helpers/utils.h"
#include "detection/yoloDetector.h"
#include <boost/range/iterator_range.hpp>
#include <boost/program_options.hpp>
#include <helpers/csvhandler.h>
#include <thread>
#include <helpers/parallelHandler.h>
#include <helpers/BlockingQueue.cpp>

#include "logging/logging.h"

namespace fs = boost::filesystem;
namespace po = boost::program_options;


void print_arguments(po::variables_map vm) {
  printf("Using following arguments: \n\n");
  for (const auto &it : vm) {
    std::cout << it.first.c_str() << " ";
    auto &value = it.second.value();
    if (auto v = boost::any_cast<int>(&value))
      std::cout << *v << std::endl;
    else if (auto v = boost::any_cast<fs::path>(&value))
      std::cout << *v << std::endl;
    else if (auto v = boost::any_cast<float>(&value))
      std::cout << *v << std::endl;
    else
      std::cout << "error \n";
  }
  printf("\n");
}

/**
 * Priority order is the same as argument order
 * @param image_path
 * @param image_dir
 * @param image_list_path
 * @param image_csv
 * @return
 */
std::vector<fs::path>
parse_img_paths(fs::path image_path, fs::path image_dir,
                fs::path image_list_path) {
  std::vector<std::string> allowedFormats{".jpg", ".JPG", ".png", ".PNG", ".tiff", ".TIFF", ".gif", ".GIF", ".jpeg",
                                          ".JPEG"};
  std::vector<fs::path> result;
  bool validFound=false;

  if (fs::exists(image_path)  && !fs::is_directory(image_dir)) {
    if (std::find(std::begin(allowedFormats), std::end(allowedFormats),
                  fs::extension(image_path)) != std::end(allowedFormats)) {
      pqry::Logging::say(__FILE__, __LINE__, "Valid Image found %s. \n",image_path.string().c_str());
      result.push_back(image_path);
      validFound=true;
    }
  } else if (fs::exists(image_dir) && fs::is_directory(image_dir)) {

    for (auto &entry :
            boost::make_iterator_range(fs::directory_iterator(image_dir), {})) {
      if (std::find(std::begin(allowedFormats), std::end(allowedFormats),
                    fs::extension(entry.path())) != std::end(allowedFormats)) {
        pqry::Logging::say(__FILE__, __LINE__, "Valid Image found: %s. \n", entry.path().string().c_str());
        result.push_back(entry.path());
        validFound=true;
      }
    }
  } else if (fs::exists(image_list_path)) {
    pqry::Logging::say(__FILE__, __LINE__, "Valid ImageList found. \n");
    fs::ifstream file(image_list_path);
    std::string image_path_str;
    while (getline(file, image_path_str)) {
      fs::path img_path(image_path_str);
      if (std::find(std::begin(allowedFormats), std::end(allowedFormats),
                    fs::extension(img_path)) != std::end(allowedFormats)) {
        pqry::Logging::say(__FILE__, __LINE__, "Valid Image found %s. \n",image_path.string().c_str());
        result.push_back(img_path);
        validFound=true;
      }
    }
  }
  if(!validFound) {
    pqry::Logging::say(__FILE__, __LINE__, "No valid image paths found. \n");
  }

  return result;
}


/**
 * Takes argc and argv and parses them into the defined variables
 *
 * @param argc
 * @param argv
 * @param vm - boost::option_programs uninitialzed(unstored) pointer
 * @param config_path - Path to config.ini
 * @param visual_dir_path - Directory for Images with detection overlay
 * @param nthreads - Num of Threads for parallel calculations
 * @param threshold - Threshold for detections
 * @param image_path - Path to image to detect
 * @param image_dir - Path to directory with images to detect
 * @param image_list_path - Path to text file with a list of images to detect
 * @param image_csv - Path to image detection in csv to visualize
 * @param csv_output_path - Path to output csv file with detections
 * @return
 */
bool parseArguments(int argc, char **argv, po::variables_map *vm, fs::path *config_path, fs::path *visual_dir_path,
                    int *nthreads, float *threshold, fs::path *image_path,
                    fs::path *image_dir, fs::path *image_list_path, fs::path *image_csv, fs::path *csv_output_path) {


  int default_threads=1;
  int default_threshhold=0.4;

  po::options_description desc("Allowed options");
  desc.add_options()
          // First parameter describes option name/short name
          // The second is parameter to option
          // The third is description
          ("help,h", "Print usage message")
          ("config_path,c", po::value(config_path)->required(), "Path to config.ini")
          ("num_threads,n", po::value(nthreads)->default_value(default_threads), "Num of Threads for parallel calculations")
          ("threshold,t,", po::value(threshold)->default_value(default_threshhold), "Threshold for detections")
          ("visual_dir_path,y", po::value(visual_dir_path), "Directory for Images with detection overlay")
          ("image_path,i", po::value(image_path), "Path to image to detect")
          ("image_dir,d", po::value(image_dir), "Path to directory with images to detect")
          ("image_list_path,l", po::value(image_list_path), "Path to text file with a list of images to detect")
          ("image_csv_path,s", po::value(image_csv), "Path to image detection in csv to visualize")
          ("csv_output_path,o", po::value(csv_output_path), "Path to output csv file with detections");

  po::store(parse_command_line(argc, (const char *const *) argv, desc), (*vm));

  if (vm->count("help")) {
    std::cout << desc << std::endl;
    return false;
  }

  /**
   * Check if the Input might be correct.
   */
  print_arguments((*vm));
  po::notify(*vm);

  if (!vm->count("image_path") && !vm->count("image_dir") && !vm->count("image_list_path") &&
      !vm->count("image_csv_path")) {
    pqry::Logging::panic(__FILE__, __LINE__,
                         "You have to define one input. Either image_path, image_dir, image_list_path or image_csv_path");
    return false;
  }

  if(vm->count("image_path")){
    if(fs::is_directory(*image_path)){
      pqry::Logging::panic(__FILE__, __LINE__,"Image path is a directory. Please check usage of -i and -d");
      return false;
    }
    if(!fs::exists(*image_path)){
      pqry::Logging::panic(__FILE__, __LINE__,"Image path doesnt exist");
      return false;
    }
  }

  if(vm->count("image_dir")){
    if(!fs::is_directory(*image_dir)){
      pqry::Logging::panic(__FILE__, __LINE__,"Image Dir is not a directory. Please check usage of -i and -d");
      return false;
    }
    if(!fs::exists(*image_dir)){
      pqry::Logging::panic(__FILE__, __LINE__,"Image Dir doesnt exist");
      return false;
    }
  }

  if(*nthreads<=0){
    *nthreads=default_threads;
    pqry::Logging::say(__FILE__, __LINE__,"Num threads has to be >0. Corrected to default: %u ",default_threads);
  }

  if(*threshold<=0) {
    *threshold = default_threshhold;
    pqry::Logging::say(__FILE__, __LINE__, "Threshold has to be >0. Corrected to default: %u ", default_threshhold);
  }
  
  if(vm->count("visual_dir_path")){
    if(!fs::is_directory(*visual_dir_path)){
      pqry::Logging::panic(__FILE__, __LINE__,"Visual Dir (-y) is not a directory.");
      return false;
    }
    if(!fs::exists(*visual_dir_path)){
      pqry::Logging::panic(__FILE__, __LINE__,"Visual Dir doesnt exist");
      return false;
    }
  }

  return true;
}

void load_image_and_save_with_overlay(fs::path img_path, std::vector<Detection> detections, fs::path visual_dir_path,
                                      std::vector<std::string> class_names) {
  const cv::Mat image = cv::imread(img_path.string(),
                                   CV_LOAD_IMAGE_COLOR);
  cv::Mat img_wOverlay = detection::overlay(image, detections,
                                            class_names);
  fs::path img_output = visual_dir_path / boost::filesystem::path(
          img_path.stem().string() + "_prediction" +
          img_path.extension().string());
  pqry::Logging::say(__FILE__, __LINE__, "Writting image: %s",
                     img_output.string());
  cv::imwrite(img_output.string(), img_wOverlay);
}

/**
 * This methods parses the CSV Input file, loads the images, creates the overlay and saves them on disk
 *
 * @param input_csv_path Path to csv input file
 * @param class_names  Vector to translate class index to class name
 * @param visual_dir_path Directory to store the image with overlay
 */
void visualize_csv_detections(fs::path input_csv_path, std::vector<std::string> class_names, fs::path visual_dir_path) {
  pqry::Logging::say(__FILE__, __LINE__, "CSV input found. Reading it...");
  std::fstream csv_input_stream;
  csv::open_CSVFile_to_read(input_csv_path, &csv_input_stream);

  std::vector<std::pair<fs::path, std::vector<Detection>>> data = csv::parseCSVinput(&csv_input_stream);

  for (auto &pair:data) {
    fs::path img_path = pair.first;
    std::vector<Detection> detections = pair.second;
    load_image_and_save_with_overlay(img_path, detections, visual_dir_path, class_names);
  }
  csv_input_stream.close();
}

/**
 * This method is queued into the threadpool for every image. The results are stored in result_pairs
 *
 * @param idx Index to use to store the results.
 * @param yoloQueue BlockingQueue with initialized Yolo Networks
 * @param current_img_path Path for image
 * @param result_pairs Reference to vector to store results.
 */
void calculateDetections(int idx, maux::BlockingQueue<Yolo> &yoloQueue, fs::path &current_img_path,
                         std::vector<std::pair<fs::path, std::vector<Detection>>> &result_pairs) {
  pqry::Logging::say(__FILE__, __LINE__, "Processing: %s",
                     current_img_path.string());

  Yolo yolo = yoloQueue.wait_pop();

  const cv::Mat image = cv::imread(current_img_path.string(),
                                   CV_LOAD_IMAGE_COLOR);
  if (!image.data)
    pqry::Logging::panic(__FILE__, __LINE__,
                         "Could not open or find the image: %s",
                         current_img_path.string());

  //Detecting Objects

  std::vector<Detection> detections = yolo.detect(image);
  result_pairs.at(idx).first = current_img_path;
  result_pairs.at(idx).second = detections;

  pqry::Logging::say(__FILE__, __LINE__, "Adding to results: %s",
                     current_img_path.string());
  yoloQueue.push(yolo);
}

/**
 * Main Method. It does one of two things:
 *
 * 1) It takes images as input, calculates the object detections and creates
 * images with overlays and one CSV File with the results
 *
 * 2) Takes an CSV file as input, load the images and generates the overlay to control the calculations.
 *
 * @param argc
 * @param argv
 * @return 0/-1
 */
int main(int argc, char **argv) {

  fs::path config_path, visual_dir_path, image_path, image_dir, image_list_path, image_csv_path, csv_output_path;
  int nthreads;
  float threshold;


  /*
   * Load and check the arguments. Throws Logging:panics
   * if something is not already.
   */
  po::variables_map vm;
  if (!parseArguments(argc, argv, &vm, &config_path, &visual_dir_path,
                      &nthreads, &threshold, &image_path, &image_dir,
                      &image_list_path, &image_csv_path, &csv_output_path))
    return 0;

  bool has_csv_input = vm.count("image_csv_path");
  bool has_csv_output = vm.count("csv_output_path");
  bool has_vis_output = vm.count("visual_dir_path");

  /*
  * If an input CSV file is defined, we ignore everything else and process
  * only the file. Do not initialize the YOLO Network. Read only class names.
  */
  if (has_csv_input) {
    Yolo yolo;
    yolo.loadClassNames(config_path);
    visualize_csv_detections(image_csv_path, yolo.getClassNames(), visual_dir_path);
    return 0;
  }

  /*
   * Initializing a blocking queue with initialized yolo Detectors
   * to avoid raceconditions during the parallel calculations.
   *
   * Get a copy of the class_names for diff usages during the pipeline
   * without alterning the yoloQueue anymore.
   */
  maux::BlockingQueue<Yolo> yoloQueue;
  std::vector<std::string> class_names;
  for (int k = 0; k < nthreads; ++k) {
    Yolo yolo;
    yolo.init(config_path);
    class_names=yolo.getClassNames();
    yoloQueue.push(yolo);
  }

  /*
   * Preparing the ParallelHandler with the num of threads.
   * Parsing the image paths our of image_path,image_dir or image_list_path.
   * If the input source is image_dir, only png and jpeg images are taken into count.
   *
   * The results are storen in the result_pairs vector.
   */
  std::vector<fs::path> img_paths = parse_img_paths(image_path, image_dir, image_list_path);

  ParallelHandler loopHandler;
  loopHandler.init(nthreads);

  std::vector<std::pair<fs::path, std::vector<Detection>>> result_pairs(img_paths.size());

  for (int j = 0; j < img_paths.size(); ++j) {
    fs::path current_img_path = (fs::path) img_paths.at(j);
    (*loopHandler.getLoopService()).post(
            boost::bind(calculateDetections, j, boost::ref(yoloQueue), current_img_path, boost::ref(result_pairs)));
  }

  //This call blocks the main thread until the threadpool has no more work queued.
  loopHandler.executeAll();


  /*
   * Open CSV output stream, iterate over the results and store them.
   * If visualization is desired, the images will be loaded and store into the visual_dir_path
   */

  std::fstream csv_output_stream;
  if (has_csv_output)
    csv::open_or_create_CSVFile_to_write(csv_output_path, &csv_output_stream);

  for (auto &pair:result_pairs) {
    fs::path current_img_path = pair.first;
    std::vector<Detection> detections = pair.second;

    std::vector<Detection> thresh_detections = detection::above_threshold(
            detections, threshold);

    //Create Image with Overlay
    if (has_vis_output) {
      load_image_and_save_with_overlay(current_img_path, thresh_detections, visual_dir_path, class_names);
    }
    //Output to CSV if needed
    if (has_csv_output)
      csv::write_detections(&csv_output_stream, current_img_path, thresh_detections);
  }

  if (has_csv_output) csv_output_stream.close();
  return 0;
}