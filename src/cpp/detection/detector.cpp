//
//


#include "detector.h"

/**
 * Initializes the Detector with the config file. Important are the .names Files, .weights File and .cfg File
 * @param config_path Path to config.ini
 */
void IDetector::init(const boost::filesystem::path &config_path) {
  boost::property_tree::ptree pt;
  boost::property_tree::ini_parser::read_ini(config_path.string(), pt);

  boost::filesystem::path names_path(pt.get<std::string>("NetworkData.Names"));
  boost::filesystem::path weights_path(pt.get<std::string>("NetworkData.Weights"));
  boost::filesystem::path net_path(pt.get<std::string>("NetworkData.Network"));

  if(!boost::filesystem::exists(names_path))
    pqry::Logging::panic(__FILE__, __LINE__,"ERROR: Names file doesnt exists: %s ! Have you updated the config.ini? \n",names_path.string().c_str());
  pqry::Logging::say(__FILE__, __LINE__,"Names Path: %s \n",names_path.string().c_str());

  if(!boost::filesystem::exists(weights_path))
    pqry::Logging::panic(__FILE__, __LINE__,"ERROR: Weights file doesnt exists: %s ! Have you updated the config.ini?\n",weights_path.string().c_str());
  pqry::Logging::say(__FILE__, __LINE__,"Weights Path: %s \n",weights_path.string().c_str());

  if(!boost::filesystem::exists(net_path))
    pqry::Logging::panic(__FILE__, __LINE__,"ERROR: Network file doesnt exists: %s ! Have you updated the config.ini?\n",net_path.string().c_str());
  pqry::Logging::say(__FILE__, __LINE__,"Network Path: %s \n",net_path.string().c_str());

  // Load Network with Weights
  external_load_network_weights(net_path.string().c_str(),weights_path.string().c_str(),&net);

  loadClassNames(config_path);

  assert(net.layers->classes==class_ids_to_name.size());
};

void IDetector::loadClassNames(const boost::filesystem::path &config_path){
  boost::property_tree::ptree pt;
  boost::property_tree::ini_parser::read_ini(config_path.string(), pt);

  boost::filesystem::path names_path(pt.get<std::string>("NetworkData.Names"));
  // Parse names file to vector
  boost::filesystem::ifstream file(names_path);
  std::string name;
  while(getline(file, name)){
    class_ids_to_name.push_back(name);
  }
}

std::string IDetector::getClassNamePerID(int idx){
  return class_ids_to_name.at(idx);
}

std::vector<std::string> IDetector::getClassNames(){
  return class_ids_to_name;
}

namespace detection {

    /**
     * Generates an image with a visual representation of the detections.
     * @param im Original Image
     * @param detections Vector with detections
     * @param class_names Vector with Mapping class index -> class name
     * @return Image with Overlay
     */
    cv::Mat overlay(const cv::Mat& im, const std::vector<Detection>& detections,const std::vector<std::string> class_names){
      cv::Mat image_wOverlay(im);
      int thickness = 2;
      cv::Scalar color(0,100,0);

      for (std::vector<Detection>::const_iterator det_it = detections.begin();
           det_it != detections.end(); ++det_it) {

          Detection det = *det_it;
          cv::rectangle(image_wOverlay,det.bbox,color,thickness);
          if(det.class_id !=-1)
            cv::putText(image_wOverlay, class_names.at(det.class_id), cvPoint(det.bbox.x,det.bbox.y-5),
                cv::FONT_HERSHEY_COMPLEX_SMALL, 1, color, 1, CV_AA);
      }
      return image_wOverlay;
    };

    /**
     * Filters detection based on a given threshold. If det.score > threshold -> Accepted
     * @param detections Vector of detections
     * @param threshold  Threshold value
     * @return Vector with detections filtered.
     */
    std::vector<Detection> above_threshold(const std::vector<Detection>& detections, double threshold){
      std::vector<Detection> result;
      for (std::vector<Detection>::const_iterator det_it = detections.begin();
           det_it != detections.end(); ++det_it) {
          if(det_it->score>threshold) result.push_back(*det_it);
      }
      return result;
    };

}  // namespace detection