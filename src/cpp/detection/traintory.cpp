/*
* Copyright 2017 Parquery AG <Hans Hardmeier>
*/

#include <iostream>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include "logging/logging.h"
extern "C"{
  void train_full_detector(char *train_images, char *cfgfile, char *weightfile, char *backup_directory, int *gpus, int ngpus,int clear);
}

namespace po = boost::program_options;
namespace fs = boost::filesystem;

/**
 * Takes argc and argv and parses them into the defined variables
 *
 * @param argc
 * @param argv
 * @param vm - boost::option_programs uninitialzed(unstored) pointer
 * @param config_path - Path to config.ini
 * @param backup_dir_path - Directory for Checkpoints of training
 * @param clear - Sets network seen Value=0
 * @return
 */
bool parseArguments(int argc, char **argv, po::variables_map *vm, fs::path *config_path, fs::path *backup_dir_path,std::vector<int> *gpus, int*clear) {

  po::options_description desc("Allowed options");
  desc.add_options()
          // First parameter describes option name/short name
          // The second is parameter to option
          // The third is description
          ("help,h", "Print usage message")
          ("config_path,c", po::value(config_path)->required(), "Path to config.ini")
          ("backup_dir,b", po::value(backup_dir_path)->required(), "Path to Directory to store checkpoints")
          ("gpus,g",po::value<std::vector<int>>(gpus)->multitoken(), "Array of gpus indeces to use (i.e. 0,1,2,3)")
          ("clear,e", po::value(clear), "Sets the network seen value to 0")
          ;

  po::store(parse_command_line(argc, (const char *const *) argv, desc), (*vm));

  if (vm->count("help")) {
    std::cout << desc << std::endl;
    return false;
  }
  return true;
}

/**
 * Main Method to train a new network
 *
 * @param argc
 * @param argv
 * @return 0/-1
 *
 **/
int main(int argc, char **argv) {

  fs::path config_path, backup_dir_path;
  std::vector<int> gpus;
  int clear;

  /*
    * Load and check the arguments. Throws Logging:panics
    * if something is not already.
    */

  po::variables_map vm;
  if (!parseArguments(argc, argv, &vm, &config_path,
                      &backup_dir_path, &gpus,&clear))
    return 0;
  po::notify(vm);

  /*
   * Parse arguments to C objects
   */

  char *train_images = nullptr;
  char *cfgfile= nullptr;
  char* weightfile= nullptr;
  char * backup_directory= nullptr;

  boost::property_tree::ptree pt;
  boost::property_tree::ini_parser::read_ini(config_path.string(), pt);

  boost::filesystem::path train_img_paths(pt.get<std::string>("TrainValidData.TrainList"));
  if(!boost::filesystem::exists(train_img_paths)) pqry::Logging::panic(__FILE__, __LINE__, "Please check config.ini.  Image List doesnt exist: %s",
                                                                       train_img_paths.string());

  train_images = (char *) train_img_paths.c_str();

  boost::filesystem::path net_path(pt.get<std::string>("NetworkData.Network"));
  if(!boost::filesystem::exists(net_path)) pqry::Logging::panic(__FILE__, __LINE__, "Please check config.ini. Network File doesnt exist: %s",
                                                                net_path.string());
  cfgfile = (char*) net_path.c_str();

  backup_directory = (char*) backup_dir_path.c_str();

  boost::filesystem::path weights_path(pt.get<std::string>("NetworkData.Weights"));
  if(boost::filesystem::exists(weights_path)) weightfile = (char*) weights_path.c_str();

  /*
   * Run Loop to train network
   */

  train_full_detector(train_images, cfgfile, weightfile, backup_directory, &gpus[0],gpus.size(),clear);
  return 0;
}
