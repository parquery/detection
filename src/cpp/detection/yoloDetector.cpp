//
// Created by hanshardmeier on 19.06.17.
//


#include "yoloDetector.h"

/**
 * Detects all the objects and returns a sorted vector with them
 * @param im cv::Mat Image
 * @return Sorted Detection Vector
 */
std::vector<Detection> Yolo::detect(const cv::Mat& im){
  std::vector<Detection> detections;
  //Sort by score

  layer l = net.layers[net.n-1];
  int num_detections = l.w*l.h*l.n;
  int classes = l.classes;

  box* boxes = (box*) calloc(l.w*l.h*l.n, sizeof(box));
  float** probs = (float**) calloc(l.w*l.h*l.n, sizeof(float *));
  int j;
  for(j = 0; j < l.w*l.h*l.n; ++j) probs[j] = (float*)calloc(l.classes + 1, sizeof(float *));

  IplImage iplImage(im);

  pqry::Logging::say(__FILE__,__LINE__,"Detecting Objects with Network.");
  detect_cvMat(iplImage,&net,boxes,probs);


  pqry::Logging::say(__FILE__,__LINE__,"Transforming Detections");
  detections = box_probs_to_detection(num_detections,boxes,probs,classes,iplImage);

  if(detections.size()>0) {
    pqry::Logging::say(__FILE__, __LINE__, "Sorting Detection Vector.");
    std::sort(detections.rbegin(), detections.rend());
  }else{
    pqry::Logging::say(__FILE__, __LINE__, "No Detections found.");
  }
  Utils::free_all_ptrs((void **)probs, l.w*l.h*l.n);
  free(boxes);
  return detections;
};

/**
 * Converts the output from darknet to a more maintainable form
 * @param num
 * @param boxes
 * @param probs
 * @param classes
 * @return
 */
std::vector<Detection> Yolo::box_probs_to_detection(int num, box* boxes, float** probs, int classes,IplImage im){

  if(!boxes) pqry::Logging::panic(__FILE__,__LINE__,"Boxes are empty after Network calculations!");
  if(!probs) pqry::Logging::panic(__FILE__,__LINE__,"Probabilities are empty after Network calculations!");

  std::vector<Detection> detections_vec;
  int i;
  for(i = 0; i < num; ++i){

    Detection det;

    box b = boxes[i];

    int left  = (b.x-b.w/2.)*im.width;
    int right = (b.x+b.w/2.)*im.width;
    int top   = (b.y-b.h/2.)*im.height;
    int bot   = (b.y+b.h/2.)*im.height;

    if(left < 0) left = 0;
    if(right > im.width-1) right = im.width-1;
    if(top < 0) top = 0;
    if(bot > im.height-1) bot = im.height-1;


    cv::Rect rect(left,top,right-left,bot-top);

    det.bbox=rect;
    if(!probs[i]) pqry::Logging::panic(__FILE__,__LINE__,"Probability at %u is empty!",i);

    int cls = Utils::max_index_in_array(probs[i], classes);
    det.class_id=cls;
    det.score=probs[i][cls];

    if(det.score>0){
      detections_vec.push_back(det);
    }
  }
  return detections_vec;
}