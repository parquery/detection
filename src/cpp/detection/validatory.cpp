/*
* Copyright 2017 Parquery AG <Hans Hardmeier>
*/

#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include "logging/logging.h"

extern "C"{
  void validate_full_detector_recall(char *valid_images,char *cfgfile, char *weightfile);
}

namespace po = boost::program_options;
namespace fs = boost::filesystem;



/**
 * Takes argc and argv and parses them into the defined variables
 *
 * @param argc
 * @param argv
 * @param vm - boost::option_programs uninitialzed(unstored) pointer
 * @param config_path - Path to config.ini
 * @return
 */
bool parseArguments(int argc, char **argv, po::variables_map *vm, fs::path *config_path) {

  po::options_description desc("Allowed options");
  desc.add_options()
          // First parameter describes option name/short name
          // The second is parameter to option
          // The third is description
          ("help,h", "Print usage message")
          ("config_path,c", po::value(config_path)->required(), "Path to config.ini")
          ;

  po::store(parse_command_line(argc, (const char *const *) argv, desc), (*vm));

  if (vm->count("help")) {
    std::cout << desc << std::endl;
    return false;
  }
  return true;
}

/**
 *
 * @param argc
 * @param argv
 * @return gfgdfgfd
 */
int main(int argc, char **argv) {

  fs::path config_path;

  /*
    * Load and check the arguments. Throws Logging:panics
    * if something is not already.
    */

  po::variables_map vm;
  if (!parseArguments(argc, argv, &vm, &config_path))
    return 0;
  po::notify(vm);

  char *valid_images= nullptr;
  char *cfgfile= nullptr;
  char *weightfile= nullptr;

  boost::property_tree::ptree pt;
  boost::property_tree::ini_parser::read_ini(config_path.string(), pt);

  boost::filesystem::path valid_img_paths(pt.get<std::string>("TrainValidData.ValidList"));
  if(!boost::filesystem::exists(valid_img_paths)) pqry::Logging::panic(__FILE__, __LINE__, "Please check config.ini. Image List doesnt exist: %s",
                                                                       valid_img_paths.string());
  valid_images = (char *) valid_img_paths.c_str();


  boost::filesystem::path net_path(pt.get<std::string>("NetworkData.Network"));
  if(!boost::filesystem::exists(net_path)) pqry::Logging::panic(__FILE__, __LINE__, "Please check config.ini. Network File doesnt exist: %s",
                                                               net_path.string());
  cfgfile = (char*) net_path.c_str();

  boost::filesystem::path weights_path(pt.get<std::string>("NetworkData.Weights"));
  if(!boost::filesystem::exists(weights_path)) pqry::Logging::panic(__FILE__, __LINE__, "Please check config.ini. Weights File doesnt exist: %s",
                                                                    weights_path.string());
  weightfile = (char*) weights_path.c_str();

  validate_full_detector_recall(valid_images,cfgfile, weightfile);

//
//  if (argc < 4) {
//    printf("Usage ./validatory Data-File Cfg-File Weights \n");
//    return 0;
//  }
//  DarknetWrapper *darknetWrapper;
//  darknetWrapper = new DarknetWrapper(argv[1]);
//  darknetWrapper->validation(argc, argv);

  return 0;
}
