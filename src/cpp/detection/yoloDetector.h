//
// Created by hanshardmeier on 19.06.17.
//

#ifndef DETECTION_YOLODETECTOR_H
#define DETECTION_YOLODETECTOR_H

#include "detection/detector.h"
#include "helpers/utils.h"


extern "C" {

  void external_load_network_weights(const char *cfgfile, const char* weightfile, network* net);
  void detect_cvMat(const IplImage iplimg, network* net, box* boxes, float** probs);

}

class Yolo : public IDetector {
public:
    //From Parent:

    //void init(const boost::filesystem::path & config_path,
    //          const boost::filesystem::path & names_path);

    // Constructors

    // Destructors

    // returns detections sorted by the score
    std::vector<Detection> detect(const cv::Mat& im);
private:
    std::vector<Detection> box_probs_to_detection(int num, box* boxes, float** probs, int classes,IplImage im);
};

#endif //DETECTION_YOLODETECTOR_H
