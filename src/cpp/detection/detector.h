//
// Created by hanshardmeier on 19.06.17.
//

#ifndef DETECTION_DETECTOR_H
#define DETECTION_DETECTOR_H


#include <cstdint>
#include <vector>
#include <string>
#include <map>

#include <opencv2/opencv.hpp>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include <logging/logging.h>
#include "third_party/darknet/src/network.h"
#include "third_party/darknet/src/box.h"
//#include "third_party/darknet/src/utils.h"

extern "C" {
  void external_load_network_weights(const char *cfgfile, const char* weightfile, network* net);
}



struct Detection {
    cv::Rect bbox; //x,y are top left corner, w = width and h=height
    double score;
    int32_t class_id = -1;

    bool operator < (const Detection& str) const
    {
      return (score < str.score);
    }
};

class IDetector {

public:
    void init(const boost::filesystem::path & config_path);
    virtual std::vector<Detection> detect(const cv::Mat& im) = 0;
    std::string getClassNamePerID(int idx);
    std::vector<std::string> getClassNames();
    void loadClassNames(const boost::filesystem::path &config_path);
    ~IDetector() = default;

protected:
    std::vector<std::string> class_ids_to_name;
    network net;
};
namespace detection {
    cv::Mat
    overlay(const cv::Mat &im, const std::vector<Detection>& detections, std::vector<std::string> class_names);

    std::vector<Detection>
    above_threshold(const std::vector<Detection> &detections, double threshold);

}
#endif //DETECTION_DETECTOR_H
