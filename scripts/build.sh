#!/usr/bin/env bash

source variables.sh
CURRENT_DIR=$PWD

mkdir -p ${BUILD_DIR}

cmake -H../src/cpp -B${BUILD_DIR}

cd ${BUILD_DIR}
make -j5
cd ${CURRENT_DIR}

