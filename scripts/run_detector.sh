#!/usr/bin/env bash

source variables.sh

${BIN_DIR}/detectory -t 0.2 -n 2  -c ${CONFIG_INI} -o ${PROJ_DIR}/results/output.csv -y ${PROJ_DIR}/results -i ${PROJ_DIR}/data/imgs/person.jpg
