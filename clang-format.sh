#!/bin/bash

CLANGBIN="$PQRY_DEPMGMT/clang-3.9.0/bin"
if [ ! -d "$CLANGBIN" ]; then 
	echo "The clang directory is missing: $CLANGBIN. Did you install clang with depmgmt/py/install_clang.py?"
	exit 1
fi

TMPFILE=$(mktemp /tmp/production_p=ecommit_sh.XXXXXX)
CPPPTHS=`find . src/cpp -type f|grep -v src/cpp/third_party|grep '\.cpp\|\.h$'|grep -v '^obsolete'`
for f in `echo "$CPPPTHS"`; do
	$CLANGBIN/clang-format $f > $TMPFILE
	cp $TMPFILE $f

	./cpplint.py $f > /dev/null 2> $TMPFILE
	CPP_ERRS=`cat $TMPFILE|grep -v "^Skipping input"|grep -v "^Ignoring"`
	if [ ! -z "$CPP_ERRS" ]; then
    		echo "cpplint failed:"
    		cat $TMPFILE
    		exit 1
	fi
done
