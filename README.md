Object Detection (and Counting) Pipeline
========================================

This is a separate project for Parquery AG (https://www.http://parquery.com/).
This uses a customized YOLO implementation (https://pjreddie.com/darknet/yolo/) with a CPP Wrapper for better maintenance.


Overview
---------

The compiled system consists of the following files:

1. detectory - Binary to detect objects using a pre-trained model.
2. traintory - Binary to train a new model.
3. validatory - Binary to evaluate such a model.

4. config.ini - Config file containing important paths such as the class name list, weights of model and model.
5. modelX.cfg - File containing the structure of the model
6. modelXWeights.weights - File with pre-trained weights.
7. modelX.names - List to map class indexes to names.
8. (only for training/validation): train.data - File containing paths for naitive-YOLO training and validation.

Requirements
-------------

Download the depmgmt from the Bitbucket-Repository [Parquery depmgmt](git@bitbucket.org:parqueryag/depmgmt.git) and install:

1. boost-1.62 (boost_system boost_filesystem boost_program_options boost_thread)
2. opencv-2.4.13
3. curl-7.50.3

How to install the dependencies can be read in the documentation [here](https://parquery.atlassian.net/wiki/display/DE/Dependency+Management).

Compilation
-----------

After installing the dependencies run **build.sh** from the script folder to compile the program.


Training and Validation
------------------------

**traintory** and **validatory** are executables to train and validate new Weight-Files.

You can download a pre-trained weight file for the yolo.cfg Network directly from the authors websize: [Weight-File](https://pjreddie.com/media/files/yolo.weights).

You can also download other networks from [here](https://github.com/AlexeyAB/darknet/tree/master/cfg).


## How to train (to detect your custom objects):


Please when training a network from scratch, use [this initialized network](http://pjreddie.com/media/files/darknet19_448.conv.23) as an initial point.



1. Create file `yolo-custom.cfg` with the same content as in `yolo.cfg` or another [base network]((https://github.com/AlexeyAB/darknet/tree/master/cfg)) and:

  * change line batch to [`batch=64`](https://github.com/AlexeyAB/darknet/blob/master/build/darknet/x64/yolo-voc.cfg#L3)
  * change line subdivisions to [`subdivisions=8`](https://github.com/AlexeyAB/darknet/blob/master/build/darknet/x64/yolo-voc.cfg#L4)
  * change line `classes=20` to your number of objects
  * change line #237 from [`filters=125`](https://github.com/AlexeyAB/darknet/blob/master/cfg/yolo-voc.cfg#L237) to `filters=(classes + 5)*5` (generally this depends on the `num` and `coords`, i.e. equal to `(classes + coords + 1)*num`)

  For example, for 2 objects, your file `yolo-custom.cfg` should differ from `yolo.cfg` in such lines:

  ```
  [convolutional]
  filters=35

  [region]
  classes=2
  ```

2. Create file `obj.names` in the directory `data\`, with objects names - each in new line.

3. Adapt the config.ini to point to the correct files, like:

```bash
[NetworkData]
Names = /home/hanshardmeier/Development/detection/data/coco.names
Weights = /home/hanshardmeier/Development/detection/data/weights/yolo.weights 
Network = /home/hanshardmeier/Development/detection/data/yolo.cfg
[TrainValidData]
TrainList = /home/hanshardmeier/Development/darknet/trainData/coco/trainvalno5k.txt
ValidList = /home/hanshardmeier/Development/darknet/trainData/coco/5k.txt
```

4. Prepare your dataset. Put the images/labels in two folders in the following structure:

```bash
-> path/to/folder
---> images
---> labels
```

To define your train- and validation datasets create two `.txt` files with the full path to the images in every group.

To define the labels, create `.txt`-files for each `.jpg`-image-file - there and with the same name, but with `.txt`-extension, and put to file: object number and object coordinates on this image, for each object in new line: `<object-class> <x> <y> <width> <height>`

  Where: 
  * `<object-class>` - integer number of object from `0` to `(classes-1)`
  * `<x> <y> <width> <height>` - float values relative to width and height of image, it can be equal from 0.0 to 1.0 
  * for example: `<x> = <absolute_x> / <image_width>` or `<height> = <absolute_height> / <image_height>`
  * atention: `<x> <y>` - are center of rectangle (are not top-left corner)

  For example for `img1.jpg` you should create `img1.txt` containing:

```bash
  1 0.716797 0.395833 0.216406 0.147222
  0 0.687109 0.379167 0.255469 0.158333
  1 0.420312 0.395833 0.140625 0.166667
```
**Important:** The structure of the directory tree is important as the algorithm replaces `.jpg` with `.txt` and `images` with `labels` in the full_path of the images to find the labels!

5. Adapt the variables.sh file in the script folder.

6. Adapt the arguments in the file `scripts/train_network_gpus.sh`. **traintory** has the following arguments:

```bash
Allowed options:
  -h [ --help ]            Print usage message
  -c [ --config_path ] arg Path to config.ini
  -b [ --backup_dir ] arg  Path to Directory to store checkpoints
  -g [ --gpus ] arg        Array of gpus indeces to use (i.e. 0,1,2,3)
  -e [ --clear ] arg       Sets the network seen value to 0
```
*Please take note that every 200 iterations a Checkpoint is saved in `--backup_dir`. If you train for a long time, be sure to have enough space on disk.
If `Weights` is defined in config.ini it is interpreted as the last checkpoint of the calculation and will continue to train the network based on it.*


###Tips:

1. Before training:
  * set flag `random=1` in your `.cfg`-file - it will increase precision by training Yolo for different resolutions: [link](https://github.com/AlexeyAB/darknet/blob/47409529d0eb935fa7bafbe2b3484431117269f5/cfg/yolo-voc.cfg#L244)
  
  * desirable that your training dataset include images with objects at diffrent: scales, rotations, lightings, from different sides

## How to validate your network:

**validatory** has the following arguments:
```bash
Allowed options:
  -h [ --help ]            Print usage message
  -c [ --config_path ] arg Path to config.ini
```

In the *script/* Folder you will find several helpful scripts to run your pipeline more easily. Just adapt the file variables.sh and check the commands in the other Bash file (`valid_network.sh`) to understand the procedure more.


As a reference, the Training should look like this:

```bash
9002: 0.211667, 0.060730 avg, 0.001000 rate, 3.868000 seconds, 576128 images 
Loaded: 0.000000 seconds
Region Avg IOU: 0.798363, Class: 0.893232, Obj: 0.700808, No Obj: 0.004567, Avg Recall: 1.000000, count: 8 
Region Avg IOU: 0.800677, Class: 0.892181, Obj: 0.701590, No Obj: 0.004574, Avg Recall: 1.000000, count: 8
```

The validation should look like this:

```bash
7586 7612 7689 RPs/Img: 68.23 IOU: 77.86% Recall:59.00%
```

The reference weights (yolo.weights) reach:

```bash
191   814  1201 RPs/Img: 72.95  IOU: 58.65% Recall:67.78%
192   817  1205 RPs/Img: 73.05  IOU: 58.67% Recall:67.80%
193   822  1210 RPs/Img: 72.95  IOU: 58.74% Recall:67.93%
```



Detection
----------

**detectory** is the binary to detect object based on the used model. A quick `./detectory -h` reveals the usage of the command:

```
Allowed options:
  -h [ --help ]                         Print usage message
  -v [ --verbose ]                      Print out debugging messages
  -c [ --config_path ] arg              Path to config.ini
  -n [ --num_threads ] arg (=1)         Num of Threads for parallel 
                                        calculations
  -t [ --threshold ] arg (=0.400000006) Threshold for detections
  -y [ --visual_dir_path ] arg          Directory for Images with detection 
                                        overlay
  -i [ --image_path ] arg               Path to image to detect
  -d [ --image_dir ] arg                Path to directory with images to detect
  -l [ --image_list_path ] arg          Path to text file with a list of images
                                        to detect
  -s [ --image_csv_path ] arg           Path to image detection in csv to 
                                        visualize
  -o [ --csv_output_path ] arg          Path to output csv file with detections

```

Throw a look to the launching script `detectory.sh` for an example.

###Tips:

1. After training - for detection:

  * Increase network-resolution by set in your `yolo.cfg`-file (`height=608` and `width=608`) or (`height=832` and `width=832`) or (any value multiple of 32) - this increases the precision and makes it possible to detect small objects: [link](https://github.com/AlexeyAB/darknet/blob/47409529d0eb935fa7bafbe2b3484431117269f5/cfg/yolo-voc.cfg#L4)
  
    * you do not need to train the network again, just use `.weights`-file already trained for 416x416 resolution
    * if error `Out of memory` occurs then in `.cfg`-file you should increase `subdivisions=16`, 32 or 64: [link](https://github.com/AlexeyAB/darknet/blob/47409529d0eb935fa7bafbe2b3484431117269f5/cfg/yolo-voc.cfg#L3)